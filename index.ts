import fs from 'fs';
import path from 'path';
import { Stream } from 'stream';

import axios from 'axios';
import cheerio from 'cheerio';
import unzipper from 'unzipper';
import sanitize from 'sanitize-filename';

import config from './config.json';

const listUrl = process.argv[2];
if (!listUrl) {
    console.error('Error: Provide BeastSaber as an argument.');

    process.exit(0);
}

interface SongToDownload {
    title: string;
    zipUrl: string;
}

const songExists = (name: string) => {
    return fs.existsSync(path.resolve(config.target, name));
};

const downloadSong = async (song: SongToDownload) => {
    process.stdout.write(`Downloading "${song.title}"...`);

    // Check for duplicate.
    const songDirName = sanitize(song.title);
    if (songExists(songDirName)) {
        process.stdout.write(' exists, skipping.');
        console.log();

        return;
    }

    // Download and extract song zip.
    const zipFile = await axios.get<Stream>(song.zipUrl, {
        responseType: 'stream'
    });

    zipFile.data.pipe(unzipper.Extract({
        path: path.resolve(config.target, songDirName)
    }));

    // We were writing into one line so far.
    console.log();
}

const downloader = async () => {
    console.info('Fetching songs list...');

    const songsToDownload: SongToDownload[] = [];

    // Download the HTML of the song list.
    const html = await axios.get<string>(listUrl);
    const $ = cheerio.load(html.data);

    // Items with ".post" class are also in the footer.
    // We are interested only in items in the main area, that have
    // a download button.
    const songs = $('[role=main] .post');

    // Extract song titles and zips from songs HTML.
    for (let i = 0; i < songs.length; i++) {
        const song = cheerio.load(songs[i]);

        songsToDownload.push({
            title: song('.entry-title').text().trim(),
            zipUrl: song('.-download-zip').attr('href')
        });
    }

    console.info('Downloading songs...');

    for (let i = 0; i < songsToDownload.length; i++) {
        await downloadSong(songsToDownload[i]);
    }
};

downloader();
